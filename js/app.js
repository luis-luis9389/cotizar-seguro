// constructor para seguro
function Seguro(marca, anio, tipo) {
  this.marca = marca;
  this.anio = anio;
  this.tipo = tipo;
};

Seguro.prototype.cotizarSeguro = function () {
  /* 
    1 americano = 1.15
    2 asiatico = 1.05
    3 europeo = 1.35
  */
  let cantidad;
  const base = 2000;

  switch (this.marca) {
    case '1':
      cantidad = base * 1.15;
      break;
    case '2':
      cantidad = base * 1.05;
      break;
    case '3':
      cantidad = base * 1.35;
      break;
  };
  // leer año
  const diferencia = new Date().getFullYear() - this.anio;

  // cada año de diferencia hay que reducir  3% el valor del seguro
  cantidad -= ((diferencia * 3) * cantidad) / 100;

  /* si el seguro es basico se multiplica por 30% mas
    si el seguro es completo se multiplica por 50% mas
  */
  if (this.tipo === 'basico') {
    cantidad *= 1.30;
  } else {
    cantidad *= 1.50;
  };
  return cantidad;
};

// todo lo que se muestra
function Interfaz() {};

// mensaje de error
Interfaz.prototype.mostrarMensaje = function (msj, tipo) {
  const div = document.createElement('div');
  if (tipo === 'error') {
    div.classList.add('mensaje', 'error');
  } else {
    div.classList.add('mensaje', 'correcto');
  };
  div.innerHTML = `${msj}`;
  formulario.insertBefore(div, document.querySelector('.form-group'));

  setTimeout(() => {
    document.querySelector('.mensaje').remove();
  }, 3000);
};

// imprime el rasultado
Interfaz.prototype.mostrarResultado = function (seguro, total) {
  const resultado = document.getElementById('resultado');
  let marca;
  switch (seguro.marca) {
    case '1':
      marca = 'Americano';
      break;
    case '2':
      marca = 'Asiatico';
      break;
    case '3':
      marca = 'Europeo';
      break;
  };
  const div = document.createElement('div');
  div.innerHTML = `
    <p class = "header">Tu Resumen:</p>
    <p> Marca: ${marca} </p>
    <p> Año: ${seguro.anio} </p>
    <p> Tipo: ${seguro.tipo} </p>
    <p> Total: ${parseInt(total)} </p>
  `;

  const spinner = document.querySelector('#cargando img');
  spinner.style.display = 'block';

  setTimeout(() => {
    spinner.style.display = 'none';
  resultado.appendChild(div);
  }, 3000);
};

// event listeners
const formulario = document.getElementById('cotizar-seguro');
formulario.addEventListener('submit', function (e) {
  e.preventDefault();
  const marca = document.getElementById('marca');
  // accediendo a valores de options
  const marcaSelecccionada = marca.options[marca.selectedIndex].value;

  const anio = document.getElementById('anio');
  // accediendo a valores de options
  const anioSeleccionado = anio.options[anio.selectedIndex].value;

  //lee valor de radio button
  const tipo = document.querySelector('input[name="tipo"]:checked').value;

  // crear instancia de interfaz
  const interfaz = new Interfaz();
  //revisar campos vacios
  if (marcaSelecccionada === '' || anioSeleccionado === '' || tipo === '') {
    // interfaz imprimiendo un error
    interfaz.mostrarMensaje('Faltan datos, revisa el formulario y prueba de nuevo', 'error');
  } else {
    // limpiar resultados anteriores
    const resultados = document.querySelector('#resultado div');
    if (resultados != null) {
      resultados.remove();
    }
    // instanciar seguro y mostrar interfaz
    const seguro = new Seguro(marcaSelecccionada, anioSeleccionado, tipo);
    // cotizar seguro
    const cantidad = seguro.cotizarSeguro();
    // mostrar resultado
    interfaz.mostrarResultado(seguro, cantidad);
    interfaz.mostrarMensaje('Cotizando...', 'correcto');
  }
});


// generar 20 años
const max = new Date().getFullYear(),
  min = max - 20;

// select generar años
const selectAnios = document.getElementById('anio');
for (let i = max; i > min; i--) {
  let option = document.createElement('option');
  option.value = i;
  option.innerHTML = i;
  selectAnios.appendChild(option);
};